<?php

$link = mysqli_connect("localhost","manager","p@ssw0rd", "CM");

if( isset($_POST['getcalls']) || $_GET['getcalls'] ){

	exec("/usr/sbin/rasterisk -rx 'cdr show active' 2>&1|egrep -ve 'Asterisk ending|Channels with Call|LastApp|-----------'", $ret,$r);  
	exec("/usr/sbin/rasterisk -rx 'core show calls' 2>&1 |grep proc|awk  '{print($1)}'", $ret2,$r);
   	 header("Content-type: application/json");
	 echo json_encode( array('cnt'=> count($ret)-1, 'total' => $ret2 ) ) ;

     exit;
  }

 if( isset( $_GET['country'] )){

    $country =  $_GET['country'] ;
    $cli  = $_GET['cli'];
    $percent = $_GET['percentage'];
    if(!$cli){
      $r= mysqli_query($link,"DELETE FROM cmap WHERE country_prefix = '{$country}' ");
    }else{
      $r = mysqli_query($link,"UPDATE cmap SET cli_map = '{$cli}',
	   		            percent = 0$percent 
				    WHERE country_prefix = '{$country}' ") or die(mysqli_query());
      if(!mysqli_affected_rows() ){
	 mysqli_query($link,"INSERT INTO cmap(country_prefix, percent, cli_map)
			     VALUES('{$country}', 0{$percent}, '{$cli}' )") or die( mysqli_error() );
      }
    }  
    header("Location: api.php");
    exit;
 }

 if(isset($_GET['getdata'])){
    $json = array();	 
    $data = array();
    $res = mysqli_query($link, "SELECT country_prefix, percent, counter_matched, 
				       concat(counter_mapped,' (', round((counter_mapped*100)/counter_matched),'%)' ) as mapped,
				 total_counter,
				 cli_map
                                FROM cmap") ;
    while($row = mysqli_fetch_assoc($res) )
	 $data[] = $row;

    $json['data'] = $data;
    $json['draw'] = $_GET['draw'] ? $_GET['draw'] : 1;
    $json['recordsTotal'] = count($data);
    $json['recordsFiltered'] = count($data);
    header('Content-type:application/json;charset=utf-8');
    echo  json_encode($json);
    exit;
 }	


?>
<html>
<head>

  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" >
  <script type="text/javascript" src='https://code.jquery.com/jquery-3.3.1.js'></script>
  <script type="text/javascript" src='https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js'></script>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
 
  
</head>
<body>

<div class=container style='margin:auto;'>
   <h2>CallER ID Manager </h2>
<table style='width:100%' class='responsive'>
   <form class='form-control' >
	<tr><td> End Point:</td><td> http://167.172.109.151/api.php?</td></tr>
	<tr><td> country=</td><td><input class='form-control' name='country' value='44'></td></tr>
	<tr><td> &cli=<br><small>( empty cli to delete it)</small></td><td><input class='form-control' name=cli value='441234123,44321321,44112233,44332211'><td></td></tr>
	<tr><td> &percentage=</td><td nowrap><input class='form-control' name=percentage value=100></td></tr>
	<tr><td></td><td>  <input type=submit class='btn btn-success'>   </td></tr>
   </form>
</table>

</div>


<div class="container" style='width:80%;margin:auto;'>
   <div class='row alert '><span class='col-sm-2'> API Example:</span> 
		   <span class='col-sm-10'>http://167.172.109.151/api.php?country=447731&cli=999,888,777&percentage=50 </span>
   </div>
  <p class='row alert alert-success ' style='width:100%;white-space:nowrap'>
    <span class='col-sm-3 font-bold '>Calls: </span>
    <span class='col-sm-8' id='cc' > loading... </span>
   </p>


  
   <p class='row alert alert-info ' style='width:100%;white-space:nowrap'>
    <span class='col-sm-3 font-bold text-primary'>Current Outbound  Dialplan: </span>
    <span class='col-sm-8'> <?php echo  mysqli_fetch_assoc(mysqli_query($link,"SELECT concat('exten => ',exten,',1,',app,'(',appdata,')') as dp FROM outbound LIMIT 1"))['dp']; ?> </span>
   </p>

   <p class='row alert alert-info ' style='width:100%;white-space:nowrap'>
    <span class='col-sm-3'>Current Authorized PEERs(realtime) :</span>
    <span class='col-sm-8'><?php echo  mysqli_fetch_assoc(mysqli_query($link,"SELECT concat('exten => ',exten,',1,',app,'(',appdata,')') as dp FROM peers LIMIT 1"))['host']; ?> </span>
   </p>

 
  <p class=' row alert alert-info table-responsive'>
    <table id="data" class="display nowrap table-responsive" style="width:100%">
   <thead>
	    <tr>
                <th>Country pref</th>
                <th>Percent,%</th>
                <th> Matched cnt</th>
		<th> Mapped cnt(%)</th>
		<th>Total Calls</th>
		<th>Mapping rules</th>
            </tr>
        </thead>
    </table>
   </p>


 <script type='text/javascript'>
  $(document).ready(function() {
     $('#data').DataTable( {
          "processing": true,
          "serverSide": true,
	  "ajax": "api.php?getdata=1",
	   "dom": 'rtip',
	   "aoColumns":[
  		  { mData: 'country_prefix'},
  		  { mData: 'percent'},
                  { mData: 'counter_matched'},
                  { mData: 'mapped'},
                  { mData: 'total_counter'},
  		  { mData: 'cli_map'}
 	   ]
     });
  });

 setInterval(function(){
	 $.post('', {'getcalls':1}, function(data){ $('#cc').html( data['cnt'] +' <div style="float:right">Total: ' +data['total'] + "</div>"); }, 'json' );

 },1000);

 </script>

</body>
</html>
