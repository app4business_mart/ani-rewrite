<?php 
ini_set('display_errors', 1);
error_reporting(1);

//$SIPCONF = "/etc/asterisk/sip.conf";
$counters = array();

use PAMI\Client\Impl\ClientImpl as PamiClient;  
use PAMI\Message\Event\EventMessage; 
use PAMI\Listener\IEventListener;  
use PAMI\Message\Event\ExtensionStatusEvent;
use PAMI\Message\Event\BridgeEvent;
use PAMI\Message\Event\DialEvent;
use PAMI\Message\Event\CELEvent;
use PAMI\Message\Event\DialBeginEvent;
use PAMI\Message\Event\DialEndEvent;
use PAMI\Message\Event\NewstateEvent;
use PAMI\Message\Event\StatusCompleteEvent;
use PAMI\Message\Event\StatusEvent;
use PAMI\Message\Action\HangupAction;
use PAMI\Message\Action\SetVarAction;
use PAMI\Message\Action\GetVarAction;
use PAMI\Message\Action\SIPShowRegistryAction;
use PAMI\Message\Action\CoreShowChannelsAction;
use PAMI\Message\Action\MessageSendAction;

require_once("vendor/autoload.php");


$pamiClientOptions = array(
    'log4php.properties' => __DIR__ . '/log4php.properties',    
    'host' => 'localhost',  
    'scheme' => 'tcp://',  
    'port' => 5038,  
    'username' => 'pbx-manager-dev',  
    'secret' => '92jdf3hfdf',  
    'connect_timeout' => 100,  
    'read_timeout' => 10000  
    );

   $link = mysqli_connect("localhost","manager","p@ssw0rd", "CM");

   $pamiClient = new PamiClient($pamiClientOptions);  
   $pamiClient->open();  


  $pamiClient->registerEventListener(
     function (EventMessage $event) {
	GLOBAL $conf;

        $last_event = $event->getKeys();
        $event_type = $last_event["event"];
     

//      echo " EventType flown: $event_type\r\n";

        preg_match("/SIP\/(.*)\-/", $last_event['channel'] , $m );
	$PEER = $m[1];

        switch (true) {

            case $event_type == 'DialState':
                 DialState($last_event);
                 break;

	    case $event_type == 'VarSet' && $last_event['variable'] == 'SIPCALLID' :
    		 CallStartEvent($last_event,$PEER);		
                 break;

            case $event_type == 'Newchannel':
                 //NewChannelEvent( $last_event, $PEER);
		 break;
	 
	    case $event_type == 'DialEnd' :
   	        // CallEndEvent($last_event,$PEER);

            default:
                # code...
                break;
        }
      }
  );  



$running = true; 

echo "Go ..!\n";

  while($running) {
    usleep(500);
    try {
      $pamiClient->process();
    } catch (Exception $e) {
	    echo "Throw Exception:" . $e->getMessage() . "\r\n" ;   
	    exit;
    }
     
  }  

  $pamiClient->close();  






function NewChannelEvent($event,$PEER){ 
   GLOBAL $conf;
   //$wfr=$conf[$PEER]['waitforring'];
   echo "     New call  for $PEER  {$event['channel']}  \r\n";
   
}

function CallEndEvent($event, $PEER){
    GLOBAL $conf;
    GLOBAL $counters;
   //  print_r($event);
   echo "  CALL END-- - > > Peer: $PEER; Status: $status \r\n";
}

function CallStartEvent($event, $PEER){
  GLOBAL $link;
  GLOBAL $pamiClient;
    //
    //print_r($event);
    // 
  // Only NEw call (First LEG)
  if( $event['uniqueid'] == $event['linkedid'] ){
    $EXTEN  = $pamiClient->send( new GetVarAction('EXTEN', $event['channel'] ))->getKeys();
    $CALLER =  $pamiClient->send( new GetVarAction('CALLERID(num)', $event['channel'] ))->getKeys() ;
    $src = $CALLER['value'];
    $dst = $EXTEN['value'];

    $r = mysqli_query($link, "UPDATE cmap SET total_counter = ifnull(total_counter,0) + 1");
    $calls = mysqli_fetch_assoc(mysqli_query($link, "SELECT total_counter as tc FROM cmap LIMIT 1"))['tc'];

    $pref_match = mysqli_query($link,"SELECT * FROM cmap 
				 WHERE '{$dst}' LIKE concat(country_prefix,'%')
				 ORDER BY length(country_prefix)
				 LIMIT 1");


    if( $row =  $pref_match->fetch_assoc() ){
	    mysqli_query($link, "UPDATE cmap SET counter_matched = ifnull(counter_matched,0) + 1 WHERE id = {$row['id']}");
	    $mapped_p = ($row['counter_matched'] > 0)? round( ($row['counter_mapped'] *100 )/ $row['counter_matched'] ) : 0;
	    $matched = "({$row['country_prefix']}) cnt:{$row['counter_matched']}/{$row['counter_mapped']} :{$mapped_p}% ] ";
	    if( $mapped_p < $row['percent'] ){
                   $clids = explode(',', $row['cli_map'] );
                   $new_CLI = $clids[ rand(0,count($clids)-1) ];
		   $ret = $pamiClient->send( new SetVarAction('CALLERID(num)', $new_CLI, $event['channel'] ) )->getKeys();
		   $mapped = " CID MAP:( {$src} -> {$new_CLI} ) :{$ret['response']}({$ret['message']})";
		   mysqli_query($link, "UPDATE cmap SET counter_mapped = ifnull(counter_mapped,1) + 1 WHERE id = {$row['id']}");
	    }   
    }

      echo "Total #{$calls} from[{$PEER} <{$src}>] to:$dst CLI-Matched[$matched]  CLI-Mapped[$mapped]   \r\n";

  }



}


function DialState($event){
    GLOBAL $conf;
    GLOBAL $counters;
  //  print_r($event);
    preg_match("/SIP\/(.*)\-/", $event['destchannel'] , $m );
    $PEER = $m[1];
    echo " DialState: {$PEER} newState: {$event['dialstatus']}  for {$event['destchannel']} \r\n";

}

     


function send_sms($sip_to,$sms)
{
    GLOBAL $pamiClientOptions;
    GLOBAL $pamiClient;

    $MessageSend = new MessageSendAction($sip_to,$sms);
    $result = $pamiClient->send($MessageSend);
    echo "$sms\r\n";
    print_r($result->getKeys());
    echo "\r\n";
}



